import os

def launch_analysis(data_file):
    directory= os.path.dirname( os.path.dirname(__file__))
    print(f" le chemin u dossier parent {directory}")
    path_to_file = os.path.join(directory, "data", data_file)   
    print(f" le chemin du fichier {path_to_file}") 
    with open(path_to_file, "r") as myfile:
        preview = myfile.readline()
    print("aficher le preview {}".format(preview))
def main():
    launch_analysis("SyceronBrut.xml")
        


if __name__ == "__main__":
    main()