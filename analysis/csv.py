#! /usr/bin/env python3
# coding: utf-8
import pandas as pd
import pprint
import numpy as np
import matplotlib.pyplot as plt
import os
class SetOfParlementsMembers:
    def __init__(self, name):
        self.name = name

    # def total_mps(self):
    #     return len(self.dataframe)

    def data_from_csv(self, csv_file):
        self.dataframe = pd.read_csv(csv_file, sep=';')

    def data_from_dataframe(self, dataframe):
        self.dataframe = dataframe

    def display_chart(self):
        data = self.dataframe
        # sexe_data = data["sexe"]
        femme_data = data[data.sexe == "F"]
        homme_data = data[data.sexe == "H"]
        counts = [len(femme_data),len(homme_data)]
        counts = np.array(counts)
        nb_mps = counts.sum()
        proportions = counts/nb_mps
        labels = ["femmes({})".format(counts[0]), "homme({})".format(counts[1])]

        fig, ax = plt.subplots()
        ax.axis('equal')
        plt.title('{}({} MPS)'.format(self.name,nb_mps))
        ax.pie(proportions, labels=labels, autopct="%1.1f poucents")
        plt.show()

    def split_by_political_party(self):
        result={}
        data = self.dataframe
        all_parties = data["parti_ratt_financier"].dropna().unique()

        for partie in all_parties:
            data_subset = data[data.parti_ratt_financier == partie] 
            subset = SetOfParlementsMembers("ensemble des debutees du partie: {}".format(partie))
            subset.data_from_dataframe(data_subset)
            result[partie] = subset
        return result
    def __repr__(self):
       return "le nombre e parlemtaires est: {}".format(len(self.dataframe)) 
    def __str__(self):
        names=[]
        for row_index, mp in self.dataframe.iterrows():
            names += [mp.nom]
        return (str(names))
def launch_analysis(datafile, parpartie = False, info=False, searchname=False):
    sopm = SetOfParlementsMembers("all MPS")
    # dirname = os.path.dirname(os.path.dirname(__file__))    
    sopm.data_from_csv(os.path.join("data",datafile))
    sopm.display_chart()
    # import pdb; pdb.set_trace()
    
    if parpartie:
        for partie, s in sopm.split_by_political_party().items():
            s.display_chart()
        
    if info:
        print(sopm)
    if searchname: 
        print(str(sopm))

def main():
    launch_analysis("current_mps.csv")

if __name__ == "__main__":
    main()