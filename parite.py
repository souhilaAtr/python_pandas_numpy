#! /usr/bin/env python3
# coding: utf-8
import argparse
import analysis.csv as c_an
import analysis.xml as x_an
import logging as lg 

lg.basicConfig(level= lg.DEBUG)

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--extension", help=""" Type of your analysis data "xml" or "csv" """) # -r pour la commande --extention pour le parametre
    parser.add_argument("-d", "--datafile", help=""" type du file data name with extention """ )
    parser.add_argument('-i', "--info", action= 'store_true' , help= """ information about file """)
    parser.add_argument("-p","--byparty", action='store_true', help="""displays
        a graph for each political party""")
    parser.add_argument("-s", "--searchname", action= "store_true", help = """ search mp by name """)
    return parser.parse_args()
def main():
    args = parse_arguments()  
    try:
        datafile = args.datafile
        if datafile == None:
            raise Warning('you must indicate a datafile')
    except Warning as e:
        lg.warning(e)
    else:
        # import pdb; pdb.set_trace()
        if args.extension == "csv":
            c_an.launch_analysis(datafile,args.byparty,args.info, args.searchname)#"current_mps.csv"
        elif args.extension == "xml":
            x_an.launch_analysis(datafile)#"SyceronBrut.xml"
    finally:
        lg.info('analysis is over') 
        


if __name__ == "__main__":
    main()
